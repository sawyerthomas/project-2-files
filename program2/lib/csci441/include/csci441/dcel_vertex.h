#ifndef _CSCI441_DCEL_VERTEX_H_
#define _CSCI441_DCEL_VERTEX_H_

#include<vector>
#include<csci441/dcel_face.h>
#include<csci441/dcel_edge.h>

class Vertex{
    
	//vertices hold a vector of 12 floats and one incident Edge object
	//x,y,z coords. r,g,b color values, flat normal x,y,z. smooth normal x,y,z in that order
    public:
    Edge incidentEdge;
    Vertex(std::vector<float> vertInfo) : info (12,0) {
        info.assign(vertInfo.begin(), vertInfo.end());
    }
    
    std::vector<float> info;
};

#endif