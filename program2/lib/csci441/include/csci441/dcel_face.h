#ifndef _CSCI441_DCEL_FACE_H_
#define _CSCI441_DCEL_FACE_H_

#include <vector>
#include<csci441/dcel_edge.h>
#include<csci441/dcel_vertex.h>

class Face{
    
    public:
        //faces hold one vector of flat x,y,z normal vaules and one Edge object
        std::vector<float> flatNorm;
        Edge outerComponent;

    Face(){
        }
    
    void addNorm(std::vector <float> normal){
        flatNorm.push_back(normal[0]);
        flatNorm.push_back(normal[1]);
        flatNorm.push_back(normal[2]);
    }
    
    std::vector<float> getNorm(){
        return flatNorm;
    }
};

#endif
