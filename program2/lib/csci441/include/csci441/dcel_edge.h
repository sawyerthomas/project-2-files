#ifndef _CSCI441_DCEL_EDGE_H_
#define _CSCI441_DCEL_EDGE_H_

#include<vector>
#include<csci441/dcel_face.h>
#include<csci441/dcel_vertex.h>

class Vertex;
class Face;

class Edge{

public:

    //Edges only hold other objects in the dcel for traversing around the list
    Edge *twin;
    Edge *next;
    Edge *prev;
    
    Vertex *origin;

    Face *incidentFace;

    Edge(){
        
    }

};

#endif
