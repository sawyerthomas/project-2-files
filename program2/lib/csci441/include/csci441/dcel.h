#ifndef _CSCI441_DCEL_H_
#define _CSCI441_DCEL_H_
/*
Double Connected Edge List 
as implemented by Henry King and Sawyer Thomas

Referenced the constructor at
https://github.com/4x7y/dcel/blob/master/src/geo_dcel.cpp
*/

#include <vector>
#include <algorithm>
#include<csci441/dcel_vertex.h>
#include<csci441/dcel_face.h>
#include<csci441/dcel_edge.h>

class DCEL{

public:
    //The lists of each type of object the dcel holds
    std::vector<Face> f;
    std::vector<Vertex> v;
    std::vector<Edge> u;
    std::vector<std::vector<int>> faceList;
    
    DCEL(std::vector<std::vector<float>> verts, std::vector<std::vector<float>> faces){
    	//create the faceList that tracks the faces 
    	//each vertex is involved with
//        for(int i = 0; i < verts.size(); i++){
//            for(int j = 0; j < faces.size(); j++){
//                if(std::find(faces[j].begin(), faces[j].end(), i+1) != faces[j].end()){
//                    //if the face uses vertex i, add it to i's list of faces in faceList
//                    faceList[i].push_back(j);
//                }
//            }
//        }

    	for(int i = 0; i < faces.size(); i ++){
    		std::vector<std::vector<float>> faceVerts;
    		for (int j = 0; j < 3; j ++){
    			int vertId = faces[i][j];
                faceVerts.push_back(verts[vertId - 1]);
			}
        	populate(faceVerts);    		
    	}
    }
    
    //populate the dcel with verts from the obj file, does one face at a time
    void populate(std::vector<std::vector<float>> faceVerts){
        Face face = Face();
        f.push_back(face);
        
        Edge* prevLeftEdge = nullptr;
        Edge* prevRightEdge = nullptr;
        
        //for each vertex create the vertex object, set up its incidentEdge,
        //that edge's twin, those edges incidentFace, next and prev.  Add them to the lists of objects.
        for(int i = 0; i < faceVerts.size(); i++){
            std::vector<float> vertInfo (faceVerts[i]);
            Vertex vertex = Vertex(vertInfo);
            Edge goodTwin = Edge();
            Edge evilTwin = Edge();
            
            //set up the incidentEdge of new vertex
            goodTwin.incidentFace = &face;
            goodTwin.next = nullptr;
            goodTwin.origin = &vertex;
            goodTwin.twin = &evilTwin;
            
            //set up it's twin
            //evilTwin.incidentFace = nullptr;
            evilTwin.next = prevRightEdge;
            //evilTwin.origin = nullptr;
            evilTwin.twin = &goodTwin;
            
            vertex.incidentEdge = goodTwin;
            
            //add objects to edge and vertex lists
            u.push_back(goodTwin);
            u.push_back(evilTwin);
            v.push_back(vertex);
            
            //check to see if not first pass through
            if(prevLeftEdge != nullptr){
                prevLeftEdge->next = &goodTwin;
            }
            if(prevRightEdge != nullptr){
                prevRightEdge->origin = &vertex;
            }
            
            prevLeftEdge = &goodTwin;
            prevRightEdge = &evilTwin;
        }
        
        //connect the first and last left and right edges together
        Edge eldestGoodTwin = u[0];
        prevLeftEdge->next = &eldestGoodTwin;
        
        auto second = u[1];
        Edge firstEvilTwin = second;
        firstEvilTwin.next = prevRightEdge;
        
        //set the final origin and edge of the last evilTwin and first face
        prevRightEdge->origin = &v.front();
        face.outerComponent = eldestGoodTwin;
    }
};
#endif
