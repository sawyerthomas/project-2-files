//Sawyer Thomas
//project2
//Computer Graphics
//9/22/18

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>
#include <csci441/dcel.h>

#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"



const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;
bool birdsEye=true;
int changeLocation=0;
int changePlayLocation=0;
bool adjStart=true;
float r=0;
float g=0;
float b=1;
Matrix projection;

std::vector <std::vector<float>> parsedVerts;
std::vector <std::vector<float>> parsedFaces;
std::vector<std::vector<float>> faceNorms;
std::vector<std::vector<float>> color;
std::vector<std::vector<float>> vertNorms;
std::vector<float> parsedCoords;
std::vector<std::vector<int>> faceList;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix processModel(const Matrix& model, GLFWwindow *window, const Camera& camera) {
    Matrix trans;
    
    const float ROT = 1;
    const float SCALE = .05;
    const float TRANS = .01;
    
    
    if(adjStart){
        //trans.rotate_x(90);
        trans.translate(.9,2,-3.5);
    }
    //adjStart=false;
    
    if (birdsEye){
        if(changeLocation==1){
            trans.translate(0,-2,0);
            changeLocation++;
        }
        else if(changeLocation==2){
            trans.rotate_x(90);
            changeLocation=0;
        }
        if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0,-TRANS,0); }
        else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0,TRANS,0); }
        else if (isPressed(window, GLFW_KEY_LEFT)) { trans.translate(TRANS,0,0); }
        else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.translate(-TRANS,0,0); }

    }
    
    else{
        if(changeLocation==1){
            trans.translate(0,0,2);
            changeLocation++;
        }
        else if(changeLocation==2){
             trans.rotate_x(-90);
            changeLocation=0;
        }
        if (isPressed(window, GLFW_KEY_UP)) { trans.translate(0,0,TRANS); }
        else if (isPressed(window, GLFW_KEY_DOWN)) { trans.translate(0,0,-TRANS); }
        else if (isPressed(window, GLFW_KEY_LEFT)) { trans.rotate_y(-ROT); }
        else if (isPressed(window, GLFW_KEY_RIGHT)) { trans.rotate_y(ROT); }
        else if (isPressed(window, GLFW_KEY_W)) {trans.rotate_x(-ROT);}
        else if (isPressed(window, GLFW_KEY_S)) {trans.rotate_x(ROT);}

    }
    
    // SCALE
    if (isPressed(window, '-')) { trans.scale(1-SCALE, 1-SCALE, 1-SCALE); }
    else if (isPressed(window, '=')) { trans.scale(1+SCALE, 1+SCALE, 1+SCALE); }
    
    
    //toggle camera/ birds eye view
    //hit space to switch to birds eye veiw
    static int oldSpaceState = GLFW_RELEASE;
    int newSpaceState = glfwGetKey(window, GLFW_KEY_SPACE);
    if (newSpaceState == GLFW_RELEASE && oldSpaceState == GLFW_PRESS){
        birdsEye = !birdsEye;
        changeLocation=1;
        changePlayLocation=1;
        std::cout<<"Norm Switch"<<std::endl;
    }
    oldSpaceState = newSpaceState;
    
    
    return trans * model;
}

//process input method
void processInput(Matrix& model, GLFWwindow *window, const Camera& camera) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    model = processModel(model, window, camera);
    /*
    if(adjStart){
    playerModel = processModel(playerModel, window, camera);
    }
     */
}

//process player input method
void processPlayerInput(Matrix& model, GLFWwindow *window, const Camera& camera) {
    Matrix trans;
    
    const float ROT = 1;
   // const float SCALE = .05;
   // const float TRANS = .01;
    
    //sets initial player position
    if(adjStart){
        //trans.rotate_x(90);
        trans.translate(0,-.1,-2);
    }
    adjStart=false;
    
    //view from birds eye view
    if (birdsEye){
        
        if(changePlayLocation==1){
            trans.rotate_x(90);
            changePlayLocation++;
        }
        else if(changePlayLocation==2){
            trans.translate(0,-.25,-1.85);
            changePlayLocation=0;
        }
    }
    
    else{
        //view in maze
        if(changePlayLocation==1){
            trans.translate(0,.25,1.85);
            changePlayLocation++;
        }
        else if(changePlayLocation==2){
            trans.rotate_x(-90);
            changePlayLocation=0;
        }
        else if (isPressed(window, GLFW_KEY_W)) {trans.rotate_x(-ROT);}
        else if (isPressed(window, GLFW_KEY_S)) {trans.rotate_x(ROT);}
    }
    
    //sets position
    model = trans*model;
  
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

//prints out a 2D float vector for debugging
void printTwoDVec(std::vector<std::vector<float>> iterVec, std::string name){
    
    //print out 2d vector or array
    //faceNorms is name of vector
    for (int m=0;m<iterVec.size(); m++){
        std::cout << m+1 << " "<< name<< "  ";             //change
        for (int n=0; n<iterVec[0].size(); n++){
            std::cout << iterVec[m][n]<< " ";
        }
        std::cout<< std::endl;
    }
    
}

//prints out a 2D int vector for debugging
void printTwoDVec(std::vector<std::vector<int>> iterVec, std::string name){
    
    //print out 2d vector or array
    //faceNorms is name of vector
    for (int m=0;m<iterVec.size(); m++){
        std::cout << m+1 << " "<< name<< "  ";             //change
        for (int n=0; n<iterVec[0].size(); n++){
            std::cout << iterVec[m][n]<< " ";
        }
        std::cout<< std::endl;
    }
    
}


void addFaceNormals(){
    
    //populate faceNormals from parsed faces
    for (int i=0;i<parsedFaces.size();i++)
    {
        
        //select 3 vertices around a face
        //Edge e= shapeInfo.f[i].outerComponent;
        std::vector <float> vert1 (parsedVerts[parsedFaces[i][0]-1]);
        std::vector <float> vert2 (parsedVerts[parsedFaces[i][1]-1]);
        std::vector <float> vert3 (parsedVerts[parsedFaces[i][2]-1]);
        
        
        //calculate vector 1
        std::vector <float> vec1;
        vec1.push_back(vert3[0]-vert2[0]);
        vec1.push_back(vert3[1]-vert2[1]);
        vec1.push_back(vert3[2]-vert2[2]);
        
        //calculate vector 2
        std::vector <float> vec2;
        vec2.push_back(vert1[0]-vert2[0]);
        vec2.push_back(vert1[1]-vert2[1]);
        vec2.push_back(vert1[2]-vert2[2]);
        
        //cross product of vector 1 and vector 2
        std::vector <float> normal (3,0);
        normal[0] = (vec1[1] * vec2[2]) - (vec1[2] * vec2[1]);
        normal[1] = (vec1[2] * vec2[0]) - (vec1[0] * vec2[2]);
        normal[2] = (vec1[0] * vec2[1]) - (vec1[1] * vec2[0]);
        
        
        //normalize normal vector
        float length= sqrt(normal[0]*normal[0]+normal[1]*normal[1]+normal[2]*normal[2]);
        normal[0]= normal[0]/length;
        normal[1]= normal[1]/length;
        normal[2]= normal[2]/length;
        
        
        //temporary normal vectors to be pushed into 2D vector
        std::vector<float> tempNorms;
        tempNorms.push_back(normal[0]);
        tempNorms.push_back(normal[1]);
        tempNorms.push_back(normal[2]);
        faceNorms.push_back(tempNorms);
    }
}


//adds all coordinates to a long 1D vector
void makeCoords(){
    for(int i = 0; i < parsedFaces.size();i++){
        
        //assign xyz coords
        for(int j = 0; j <3; j++){
            auto vert = parsedVerts[parsedFaces[i][j]-1];
            for(int k = 0; k <vert.size();k++){
                parsedCoords.push_back((vert[k]/100));
            }
            
            //assign color
            parsedCoords.push_back(r);
            parsedCoords.push_back(g);
            parsedCoords.push_back(b);
            
            
            //if(normToggle){
            
                //assign face norms
                for(int m = 0; m <3;m++){
                    parsedCoords.push_back(faceNorms[i][m]);
                }
        }
        
        //std::cout << "print out num "<< i <<std::endl;
    }
}

//uploads and parses the file
void parseObj(const char *filename){
    //std::cout << "in the parse method with file: " << filename << std::endl;
    std::ifstream in(filename, std::ios::in);
    if (!in)
    {
        std::cout << "Cannot open file" << filename << std::endl;
        exit(1);
        
    }
    std::string line;
    int lineNum=0;
    int vertNum=0;
    int faceNum=0;
    
    while (std::getline(in, line))
    {
        //check v for vertices
        if (line.substr(0,2)=="v "){
            std::vector <float> vertIn;
            std::istringstream v(line.substr(2));
            float x,y,z;
            v>>x;v>>y;v>>z;
            std::cout << vertNum+1 << " vert coords " << x<< " , "<< y << " , " << z << " , "<< std::endl;
            
            vertIn.push_back(x);
            vertIn.push_back(y);
            vertIn.push_back(z);
            parsedVerts.push_back(vertIn);
            vertNum++;
            
        }
        
        //check v for vertices
        if (line.substr(0,2)=="f "){
            std::vector <float> faceIn;
            std::istringstream f(line.substr(2));
            float v1,v2,v3;
            f>>v1;f>>v2;f>>v3;
            faceNum++;
            std::cout<< faceNum << " face verts " << v1<< " , "<< v2 << " , " << v3 << " , "<< std::endl;
            
            faceIn.push_back(v1);
            faceIn.push_back(v2);
            faceIn.push_back(v3);
            parsedFaces.push_back(faceIn);
            
        }
        lineNum++;
    }
}



int main(void) {
    
    
    
    GLFWwindow* window;
    
    glfwSetErrorCallback(errorCallback);
    
    /* Initialize the library */
    if (!glfwInit()) { return -1; }
    
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }
    
    /* Make the window's context current */
    glfwMakeContextCurrent(window);
    
    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    
    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }
    
    
    parseObj("/Users/Sawyer/Desktop/graphics-temp/csci441-spring2018/programs/program2/tinker.obj");
    //add face normals
    addFaceNormals();
    //printTwoDVec(faceNorms, "faceNorms");
    //add vector normals
    //addVectorNormals();
    printTwoDVec(vertNorms, "vertNorms");
    makeCoords();
    //print out coords
    for (int i=0;i<(parsedCoords.size()/9); i++){
        std::cout<< i+1 << " parsedCoords ";
        for (int h=0; h<9; h++){
            std::cout<< " " << parsedCoords[i*9+h]<< " ";
        }
        std::cout<< std::endl;
    }
    //create custom obj
    Model obj(
              CustomShape(parsedCoords).coords,
              Shader("/Users/Sawyer/Desktop/graphics-temp/csci441-spring2018/programs/program2/vert.glsl", "/Users/Sawyer/Desktop/graphics-temp/csci441-spring2018/programs/program2/frag.glsl"));
    
    
    //upload player
    r=1;g=0;b=0;
    parsedCoords.clear();
    faceNorms.clear();
    vertNorms.clear();
    parsedFaces.clear();
    parsedVerts.clear();
    faceList.clear();
    parseObj("/Users/Sawyer/Desktop/graphics-temp/csci441-spring2018/programs/program2/duck.obj");
    //add face normals
    addFaceNormals();
    //add vector normals
    //addVectorNormals();
    makeCoords();
    // make a player (hes a duck named floor)
    Model floor(
                CustomShape(parsedCoords).coords,
                Shader("/Users/Sawyer/Desktop/csci441/labs/lab6/vert.glsl", "/Users/Sawyer/Desktop/csci441/labs/lab6/frag.glsl"));
    Matrix floor_trans, floor_scale,floor_rotx, floor_roty;
    //floor_trans.translate(0, -.01, -2);
    floor_trans.translate(0, 0, 0);
    floor_scale.scale(.007, .007, .007);
    floor_rotx.rotate_x(90);
    floor_roty.rotate_y(90);
    floor.model = floor_trans*floor_scale*floor_rotx*floor_roty;

    
    // setup camera
    projection.perspective(45, 1, .01, 10);
    
    Camera camera;
    camera.projection = projection;
    camera.eye = Vector(0, 0, .01);
    camera.origin = Vector(0, 0, 0);
    camera.up = Vector(0, 1, 0);
    
    // and use z-buffering
    glEnable(GL_DEPTH_TEST);
    
    // create a renderer
    Renderer renderer;
    
    // set the light position
    Vector lightPos(3.75f, 3.75f, 4.0f);
    
    
    
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        //obj.switchNorms(normToggle);
        processInput(obj.model, window, camera);
        processPlayerInput(floor.model, window, camera);
        //processInput(player.model, window, camera, obj.model);
      
        
        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        // render the object and the floor
        renderer.render(camera, obj, lightPos);
        renderer.render(camera, floor, lightPos);
        
        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    
    glfwTerminate();
    return 0;
}

