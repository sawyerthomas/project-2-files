#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;
layout (location = 2) in vec3 aNormal;
//layout (location = 2) in vec3 fNormal;
//layout (location = 3) in vec3 sNormal;


uniform mat3 itModel;
uniform mat4 model;
uniform mat4 projection;
uniform mat4 camera;
//uniform vec1 smooth;
//uniform vec1 flat;

out vec3 ourColor;
out vec3 ourNormal;
out vec3 ourFragPos;

void main() {
    gl_Position = projection * camera * model * vec4(aPos, 1.0);
    //ourNormal = itModel * fNormal*flat + itModel*sNormal*smooth;
    ourNormal = itModel * aNormal;
    ourColor = aColor;
    ourFragPos = vec3(model * vec4(aPos, 1.0));
}
