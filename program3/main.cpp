//Sawyer Thomas
//4/26/18
//project 3
//computer graphics

#include <iostream>

#include <glm/glm.hpp>

#include "bitmap_image.hpp"
int nx=640;
int ny=480;

//screen dimensions
float l = 0;
float r = 640;
float t = 0;
float b = 480;

//background color of screen
glm::vec3 backColor (75, 156, 211);

//list of intersection distances
std::vector <float> tList;

//sets viewport
struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

//not used in this program
struct Sphere {
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0))
        : center(center), radius(radius), color(color) {
            static int id_seed = 0;
            id = ++id_seed;
        }
};
 
//generates the triangles
struct Triangle {
    int id;
    glm::vec3 p1;
    glm::vec3 p2;
    glm::vec3 p3;
    glm::vec3 color;
    
    Triangle(const glm::vec3& p1=glm::vec3(0,0,0),
             const glm::vec3& p2=glm::vec3(0,0,0),
             const glm::vec3& p3=glm::vec3(0,0,0),
           const glm::vec3& color=glm::vec3(0,0,0))
    : p1(p1), p2(p2),p3(p3), color(color) {
        static int id_seed = 0;
        id = ++id_seed;
    }
};


//generates the rays
struct Ray {
    int id;
    glm::vec3 origin;
    glm::vec3 direction;
    int intersectIdx;
    
    Ray (const glm::vec3& orig=glm::vec3(0,0,0),
           const glm::vec3& dir=glm::vec3(0,0,0)){
        direction =glm::normalize(dir);
        origin = orig;
        static int id_seed = 0;
        id = ++id_seed;
    }
};

//indicates that method "intersections" appears in code
int intersections(const std::vector<Triangle>& world, Ray ray);



//method applies phong shading to a specific pixel and object
glm::vec3 phongShade(float x, float y, float t, Triangle triangle, glm::vec3 ourColor){

    glm::vec3 u (triangle.p3.x-triangle.p1.x, triangle.p3.y-triangle.p1.y,triangle.p3.z-triangle.p1.z);
    glm::vec3 v (triangle.p2.x-triangle.p1.x, triangle.p2.y-triangle.p1.y,triangle.p2.z-triangle.p1.z);
    //glm::vec3 ourNormal=glm::cross(triangle.p3-triangle.p1, triangle.p2-triangle.p1);
    
    //sets the light position and the viewer position
    //calculates the individual triangle normals
    //glm::vec3 ourNormal=glm::cross(triangle.p1, triangle.p3);
    glm::vec3 ourNormal=glm::cross(u, v);
    
    glm::vec3 eye(0,0,0);
    glm::vec3 lightPos(3.75,2,28);
    glm::vec3 ourPos(x,y,t);
    glm::vec3 normal = glm::normalize(ourNormal);
    glm::vec3 lightDir = glm::normalize(lightPos - ourPos);
    glm::vec3 viewDir = glm::normalize(eye - ourPos);
    
    std::cout<<"normals "<< normal.x<< normal.y<< normal.z << std::endl;
    
    
    //sets strength of the lighting
    float ambientStrength = 0.8;
    float specularStrength = 0.95;
    glm::vec3 lightColor(1.0, 1.0, 1.0);
    
    glm::vec3 ambient = ambientStrength * lightColor;
    
    float diff = fmax(dot(normal, lightDir), 0.0);
    glm::vec3 diffuse = diff * lightColor;

    glm::vec3 reflectDir = glm::reflect(-lightDir, normal);
    float spec = glm::pow(fmax(dot(viewDir, reflectDir), 0.0),2);
    glm::vec3 specular = diffuse*specularStrength * spec * lightColor;
    
    //clacluates total light magnitude per pixel
    glm::vec3 result = (ambient+diffuse+specular)*ourColor;
    //std::cout<<"angle "<< -dot(normal, lightDir) << std::endl;
    return result;
}




//render each pixel with anti aliasing
void render(bitmap_image& image, const std::vector<Triangle>& world, std::vector<std::vector <Ray> >raysIn) {
    
    //iterates though every pixel
    for(int i=0; i<nx;i++){
        for(int j=0; j<ny; j++){
            //checks for intersections for each ray
            int idx0=intersections(world,raysIn[(i*ny)+j][0]);
            int idx1=intersections(world,raysIn[(i*ny)+j][1]);
            int idx2=intersections(world,raysIn[(i*ny)+j][2]);
            int idx3=intersections(world,raysIn[(i*ny)+j][3]);
            int idx4=intersections(world,raysIn[(i*ny)+j][4]);
            
            //if intersection found
            if(idx0!=-1){
                //sets color at intersection
                glm::vec3 color0 (255*world[idx0].color.x,255*world[idx0].color.y,255*world[idx0].color.z);
                glm::vec3 color1 (255*world[idx1].color.x,255*world[idx1].color.y,255*world[idx1].color.z);
                glm::vec3 color2 (255*world[idx2].color.x,255*world[idx2].color.y,255*world[idx2].color.z);
                glm::vec3 color3 (255*world[idx3].color.x,255*world[idx3].color.y,255*world[idx3].color.z);
                glm::vec3 color4 (255*world[idx4].color.x,255*world[idx4].color.y,255*world[idx4].color.z);
                glm::vec3 aliasColor;
                //takes average of 5 rays
                aliasColor.x=(color0.x+color1.x+color2.x+color3.x+color4.x)/5;
                aliasColor.y=(color0.y+color1.y+color2.y+color3.y+color4.y)/5;
                aliasColor.z=(color0.z+color1.z+color2.z+color3.z+color4.z)/5;
                
                //applies phong shading to each pixel
                glm::vec3 shade= phongShade(i,j,tList[(i*ny)+j],world[idx0],aliasColor);
                
                //colors each pixel
                rgb_t color = make_colour(shade.x,shade.y,shade.z);
                image.set_pixel(i,j,color);
            }
        }
    }
}


 //render the moving objects
 void renderMove(bitmap_image& image, const std::vector<Triangle>& moveObj, std::vector<std::vector <Ray> >raysIn) {
     int speed=1;
     //set the x and y diection and magnitude of the shape's movement
     int directionX=3;
     int directionY=8;
     
     //holds the colors to be blended
     std::vector<std::vector <float> >colorMix;
     
     //iterates through every pixel
     for(int i=directionY*2; i<nx-directionY*2;i++){
         for(int j=directionX*2; j<ny-directionX*2; j++){
             
             //five rays find the color of nearby pixels in the direction of movement
             int idx0=intersections(moveObj,raysIn[(i*ny)+j][0]);
             int idx1=intersections(moveObj,raysIn[((i+directionY)*ny)+(j+directionX)][0]);
             int idx2=intersections(moveObj,raysIn[((i-directionY)*ny)+(j-directionX)][0]);
             int idx3=intersections(moveObj,raysIn[((i+directionY*2)*ny)+(j+directionX*2)][0]);
             int idx4=intersections(moveObj,raysIn[((i-directionY*2)*ny)+(j-directionX*2)][0]);
         
             //defaults to background color
             glm::vec3 color0 (backColor.x,backColor.y,backColor.z);
             glm::vec3 color1 (backColor.x,backColor.y,backColor.z);
             glm::vec3 color2 (backColor.x,backColor.y,backColor.z);
             glm::vec3 color3 (backColor.x,backColor.y,backColor.z);
             glm::vec3 color4 (backColor.x,backColor.y,backColor.z);
             
             //assigns each color based on ray tracing
             if(idx0!=-1){
                    color0.x=255*moveObj[idx0].color.x; color0.y=255*moveObj[idx0].color.y; color0.z=255*moveObj[idx0].color.z;
             }
             if(idx1!=-1){
                 color1.x=255*moveObj[idx1].color.x; color1.y=255*moveObj[idx1].color.y; color1.z=255*moveObj[idx1].color.z;
             }
             if(idx2!=-1){
                 color2.x=255*moveObj[idx2].color.x; color2.y=255*moveObj[idx2].color.y; color2.z=255*moveObj[idx2].color.z;
             }
             if(idx3!=-1){
                 color3.x=255*moveObj[idx3].color.x; color3.y=255*moveObj[idx3].color.y; color3.z=255*moveObj[idx3].color.z;
             }
            if(idx4!=-1){
                 color4.x=255*moveObj[idx4].color.x; color4.y=255*moveObj[idx4].color.y; color4.z=255*moveObj[idx4].color.z;
            }
  
             //takes average of ray's colors
             glm::vec3 aliasColor;
             aliasColor.x=(color0.x+color1.x+color2.x+color3.x+color4.x)/5;
             aliasColor.y=(color0.y+color1.y+color2.y+color3.y+color4.y)/5;
             aliasColor.z=(color0.z+color1.z+color2.z+color3.z+color4.z)/5;
             
             //shades pixels
             if((idx0!=-1 || aliasColor.x!=75 || aliasColor.y!=156 || aliasColor.y!=211) && aliasColor.x>2){
                 //glm::vec3 shade= phongShade(i,j,tList[(i*ny)+j],moveObj[idx0],aliasColor);
                 rgb_t color = make_colour(aliasColor.x,aliasColor.y,aliasColor.z);
                 image.set_pixel(i,j,color);
             }
         }
     }
 }


//calculate the intersections of the rays
int intersections(const std::vector<Triangle>& world, Ray ray){
    std::vector <float> intersectList;
    
    float curT;
    
    //iterates though every shape
    for(int s=0; s<world.size(); s++){
        
        //solves martrix unknowns
        glm::mat3 coefMat= {world[s].p2.x-world[s].p1.x, world[s].p3.x-world[s].p1.x, -ray.direction.x,
                            world[s].p2.y-world[s].p1.y, world[s].p3.y-world[s].p1.y, -ray.direction.y,
                            world[s].p2.z-world[s].p1.z, world[s].p3.z-world[s].p1.z, -ray.direction.z
                            };
        float detMat =glm::determinant(coefMat);
        
        glm::mat3 coefMatU= {ray.origin.x-world[s].p1.x, world[s].p3.x-world[s].p1.x, -ray.direction.x,
                            ray.origin.y-world[s].p1.y, world[s].p3.y-world[s].p1.y, -ray.direction.y,
                            ray.origin.z-world[s].p1.z, world[s].p3.z-world[s].p1.z, -ray.direction.z
        };
        float detMatU =glm::determinant(coefMatU);
        
        glm::mat3 coefMatV= {world[s].p2.x-world[s].p1.x, ray.origin.x-world[s].p1.x, -ray.direction.x,
                            world[s].p2.y-world[s].p1.y, ray.origin.y-world[s].p1.y, -ray.direction.y,
                            world[s].p2.z-world[s].p1.z, ray.origin.z-world[s].p1.z, -ray.direction.z
        };
        float detMatV =glm::determinant(coefMatV);
        
        glm::mat3 coefMatT= {world[s].p2.x-world[s].p1.x, world[s].p3.x-world[s].p1.x, ray.origin.x-world[s].p1.x,
                            world[s].p2.y-world[s].p1.y, world[s].p3.y-world[s].p1.y, ray.origin.y-world[s].p1.y,
                            world[s].p2.z-world[s].p1.z, world[s].p3.z-world[s].p1.z, ray.origin.z-world[s].p1.z
        };
        float detMatT =glm::determinant(coefMatT);
       
        float u=detMatU/detMat;
        float v=detMatV/detMat;
        float t=detMatT/detMat;
    
        
        //if collision, pushes the smallest distance value onto the list
        if (v<0|| 1<v){
            intersectList.push_back(INFINITY);
        }
        else if(u<0 || (1-v)<u){
            intersectList.push_back(INFINITY);
        }
        else if(t<0){
            intersectList.push_back(INFINITY);
        }
        else{
            intersectList.push_back(t);
            
        }
    }
    
    //iterates though shapes to select the closest
    int index = 0;
    for(int i = 1; i < intersectList.size(); i++)
    {
        if(intersectList[i] < intersectList[index]){
            index = i;
            curT=intersectList[i];
        }
    }
    
    //pushes onto list of distances
    tList.push_back(curT);
    
    //returns shape index if collision occurs
    if(intersectList[index]!=INFINITY){
        return index;
    }
    else{
        return -1;
    }
}



int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(nx, ny);
    rgb_t color = make_colour(75, 156, 211);
    for( int x=0; x<nx; x++){
        for (int y=0; y<ny; y++) {
           image.set_pixel(x,y,color);
        }
    }

    //sets viewport
    Viewport myView (glm::vec2(-10,-10),glm::vec2(10,10));

    // build world
    std::vector<Triangle> world = {
        Triangle(glm::vec3(0, 8, 5),glm::vec3(-8, -2, 5),glm::vec3(-5, -2, 4), glm::vec3(1,0,0)),
        Triangle(glm::vec3(0, 8, 5),glm::vec3(-5, -2, 5),glm::vec3(-2, -2, 4), glm::vec3(1,.1,.5)),
        Triangle(glm::vec3(0, 8, 5),glm::vec3(-1, -2, 3),glm::vec3(-2, -2, 4), glm::vec3(1,1,0)),
        Triangle(glm::vec3(0, 8, 5),glm::vec3(-1, -2, 3),glm::vec3(1, -2, 3), glm::vec3(0,1,0)),
        Triangle(glm::vec3(0, 8, 5),glm::vec3(1, -2, 3),glm::vec3(3, -2, 4), glm::vec3(.5,1,0)),
        Triangle(glm::vec3(0, 8, 5),glm::vec3(3, -2, 4),glm::vec3(5, -2, 5), glm::vec3(0,1,.5)),
        Triangle(glm::vec3(0, 8, 5),glm::vec3(5, -2, 5),glm::vec3(7, -2, 6), glm::vec3(.6,0,.8)),
        Triangle(glm::vec3(0, 8, 5),glm::vec3(7, -2, 6),glm::vec3(0, -2, 8), glm::vec3(0,1,.8)),
        Triangle(glm::vec3(0, 8, 5),glm::vec3(0, -2, 8),glm::vec3(-8, -2, 8), glm::vec3(.7,1,.1)),
        //Sphere(glm::vec3(0, 0, 4), 2, glm::vec3(1,0,0)),
        //Sphere(glm::vec3(0, 0, 2), 1, glm::vec3(0,1,1)),
        //Sphere(glm::vec3(4, 2, 12), 2, glm::vec3(1,0,1)),
    };

    
    //build moving object
    std::vector<Triangle> moveObj = {
        Triangle(glm::vec3(-4, -8, 2),glm::vec3(-2, -6, 3),glm::vec3(-5, -4, 4), glm::vec3(.1,1,0)),
        Triangle(glm::vec3(5, -8, 3),glm::vec3(7, -9, 5),glm::vec3(6, -7, 4), glm::vec3(.4,.1,0)),
    };
    
    float aspectRatio = nx/ny;
    l = l*aspectRatio;
    r = r*aspectRatio;

    
    //make multiple rays per pixel and take average color
    std::vector<std::vector<Ray>> rays;
    for( int i=0; i<nx; i++){
        for (int j=ny; j>0; j--) {
            float ui = (myView.min.x) + (myView.max.x-myView.min.x)*((float)i+.5)/ nx;
            float vj = (myView.min.y )+ (myView.max.y-myView.min.y)*((float)j+.5)/ ny;
            std::vector <Ray> testRays;
            float distX=((myView.max.x-myView.min.x)/nx);
            float distY=((myView.max.y-myView.min.y)/ny);
            testRays.push_back(Ray(glm::vec3(ui,vj,0),glm::vec3(0,0,1)));
            testRays.push_back(Ray(glm::vec3(ui+distX,vj+distY,0),glm::vec3(0,0,1)));
            testRays.push_back(Ray(glm::vec3(ui-distX,vj+distY,0),glm::vec3(0,0,1)));
            testRays.push_back(Ray(glm::vec3(ui+distX,vj-distY,0),glm::vec3(0,0,1)));
            testRays.push_back(Ray(glm::vec3(ui-distX,vj-distY,0),glm::vec3(0,0,1)));
            rays.push_back(testRays);
        }
    }
    
    //renders the moving object
    renderMove(image, moveObj, rays);
    // renders the world
    render(image, world, rays);

    image.save_image("/Users/Sawyer/Desktop/graphics_proj/program3/images/SawyerFinalRender.bmp");
    std::cout << "Success" << std::endl;
}


